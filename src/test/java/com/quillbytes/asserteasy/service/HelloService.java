package com.quillbytes.asserteasy.service;

import com.quillbytes.asserteasy.model.Message;
import com.quillbytes.asserteasy.resources.MessageResource;
import com.quillbytes.asserteasy.resources.Resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

@Path("/hello")
public class HelloService {
    @Context
    private UriInfo uriInfo;

    private UriBuilder getRequestUri() {
        return uriInfo.getRequestUriBuilder();
    }

    @GET
    @Path("/{param}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response echo(@PathParam("param") String message) {
        Resource<Message> resource = MessageResource.of(message)
                .withSelfLink(getRequestUri());
        return Response.ok(resource).build();
    }

}
