package com.quillbytes.asserteasy;

import com.quillbytes.asserteasy.service.HelloService;
import org.hamcrest.CoreMatchers;
import org.hamcrest.collection.IsCollectionWithSize;
import org.hamcrest.collection.IsMapContaining;
import org.junit.Test;

import static java.lang.System.out;

public class HelloServiceTest {
    @Test
    public void testEcho() throws Exception {
        String response = AssertEasy.given()
                    .service(HelloService.class)
                .when()
                    .get("/hello/lala")
                .then()
                    .assertThat()
                        .isOk()
                        .statusIs(200)
                        .body(CoreMatchers.containsString("\"value\":\"lala\""))
                        .body("_embedded.value", "lala")
                        .body("_links", IsCollectionWithSize.hasSize(1))
                        .body("_links[0]", IsMapContaining.hasKey("href"))
                        .body("_links[0].rel", "self")
                        .body("_links[0].href", "/hello/lala")
                    .thenExtract()
                        .contentAsString();

        out.println("Content: " + response);
    }

}
