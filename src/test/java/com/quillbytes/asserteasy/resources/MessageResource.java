package com.quillbytes.asserteasy.resources;

import com.quillbytes.asserteasy.model.Message;

public class MessageResource extends Resource<Message> {

    public MessageResource(Message entity) {
        super(entity);
    }

    public static Resource<Message> of(String message) {
        return new MessageResource(new Message(message));
    }

    @Override protected String getResourceId() {
        return getEntity().getValue();
    }
}
