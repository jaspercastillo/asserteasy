package com.quillbytes.asserteasy.resources;

import com.quillbytes.asserteasy.Link;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.ws.rs.core.UriBuilder;
import java.util.LinkedList;
import java.util.List;

public abstract class Resource<T> {
    private T entity;
    private List<Link> links = new LinkedList<Link>();

    protected Resource(T entity) {
        this.entity = entity;
    }

    protected abstract String getResourceId();

    public Resource<T> withSelfLink(UriBuilder uriBuilder) {
        links.add(Link.self(uriBuilder.build()));
        return this;
    }

    @JsonProperty("_embedded")
    public T getEntity() {
        return entity;
    }

    @JsonProperty("_links")
    public List<Link> getLinks() {
        return links;
    }
}
