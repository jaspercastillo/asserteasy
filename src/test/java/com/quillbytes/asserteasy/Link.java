package com.quillbytes.asserteasy;

import java.net.URI;

public class Link {
    private String rel;
    private URI href;

    public Link(String rel, URI href) {
        this.rel = rel;
        this.href = href;
    }

    public static Link self(URI href) {
        return new Link("self", href);
    }

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public URI getHref() {
        return href;
    }

    public void setHref(URI href) {
        this.href = href;
    }

}
