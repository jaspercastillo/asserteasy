package com.quillbytes.asserteasy.spec;

import javax.ws.rs.core.MediaType;
import java.util.Collection;

public interface ExtractableSpec<ExtractType> {
    String HEADER_CONTENT_TYPE = "Content-Type";

    String contentAsString();
    <T> T path(String path);
    String pathAsString(String path);
    <T> T header(String header);
    <T> Collection<T> headers(String header);
    MediaType contentType();
    int status();
}
