package com.quillbytes.asserteasy.spec;

import java.io.File;
import java.net.URI;

public interface RequestSpec<RequestType> {
    ResponseSpec get(String uri);
    ResponseSpec get(URI uri);
    ResponseSpec post(String uri);
    ResponseSpec post(URI uri);
    ResponseSpec put(String uri);
    ResponseSpec put(URI uri);
    ResponseSpec patch(String uri);
    ResponseSpec patch(URI uri);
    ResponseSpec delete(String uri);
    ResponseSpec delete(URI uri);
    ResponseSpec head(String uri);
    ResponseSpec head(URI uri);
    ResponseSpec options(String uri);
    ResponseSpec options(URI uri);
    ResponseSpec uploadFile(String uri, File file);
    ResponseSpec uploadFile(URI uri, File file);

    RequestType getRequest();
    URI getURI();
}
