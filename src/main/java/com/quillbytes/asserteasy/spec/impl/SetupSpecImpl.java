package com.quillbytes.asserteasy.spec.impl;

import com.quillbytes.asserteasy.spec.AuthenticatedSetupSpec;
import com.quillbytes.asserteasy.spec.SetupSpec;
import com.quillbytes.asserteasy.spec.RequestSpec;

import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SetupSpecImpl implements AuthenticatedSetupSpec {
    private Set<Class<?>> serviceClasses = new HashSet<Class<?>>();
    private Set<MediaType> acceptedMediaTypes = new HashSet<MediaType>();
    private Map<String, String> parameters = new HashMap<String, String>();
    private boolean authenticationEnabled = false;

    public SetupSpec service(Class<?> serviceClass) {
        serviceClasses.add(serviceClass);
        return this;
    }

    public SetupSpec services(Class<?>... serviceClasses) {
        this.serviceClasses.addAll(Arrays.asList(serviceClasses));
        return this;
    }

    public SetupSpec accepts(String... mediaTypes) {
        for (String mediaType : mediaTypes) {
            acceptedMediaTypes.add(MediaType.valueOf(mediaType));
        }
        return this;
    }

    public SetupSpec accepts(MediaType... mediaTypes) {
        return accepts(Arrays.asList(mediaTypes));
    }

    public SetupSpec accepts(Collection<MediaType> mediaTypes) {
        acceptedMediaTypes.addAll(mediaTypes);
        return this;
    }

    public SetupSpec parameters(String... keyValueSequence) {
        throw new UnsupportedOperationException();
    }

    public SetupSpec parameters(Map<String, String> values) {
        parameters.putAll(values);
        return this;
    }

    public SetupSpec params(String... keyValueSequence) {
        return parameters(keyValueSequence);
    }

    public SetupSpec params(Map<String, String> values) {
        return parameters(values);
    }

    public SetupSpec parameter(String key, String value) {
        parameters.put(key, value);
        return this;
    }

    public SetupSpec param(String key, String value) {
        return parameter(key, value);
    }

    public AuthenticatedSetupSpec auth() {
        throw new UnsupportedOperationException();
    }

    public RequestSpec when() {
        return new RequestSpecImpl(this);
    }

    public Collection<Class<?>> getServiceClasses() {
        return Collections.unmodifiableSet(serviceClasses);
    }

    public Collection<MediaType> getAcceptedTypes() {
        return Collections.unmodifiableSet(acceptedMediaTypes);
    }

    public Map<String, String> getParameters() {
        return Collections.unmodifiableMap(parameters);
    }

    public boolean hasAuthentication() {
        return authenticationEnabled;
    }

    public AuthenticatedSetupSpec basic(String username, String password) {
        authenticationEnabled = true;
        return this;
    }
}
