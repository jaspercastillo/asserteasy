package com.quillbytes.asserteasy.spec.impl;

import com.jayway.restassured.path.json.JsonPath;
import com.quillbytes.asserteasy.spec.AssertableSpec;
import com.quillbytes.asserteasy.spec.ExtractableSpec;
import com.quillbytes.asserteasy.spec.PromiseSpec;
import com.quillbytes.asserteasy.spec.RequestSpec;
import com.quillbytes.asserteasy.spec.ResponseSpec;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.mock.MockHttpResponse;
import org.junit.Assert;

import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertThat;

public class ResponseSpecImpl implements ResponseSpec<MockHttpResponse>, AssertableSpec<MockHttpResponse>,
        PromiseSpec<MockHttpResponse>, ExtractableSpec<MockHttpResponse> {
    RequestSpec<MockHttpRequest> requestSpec;
    MockHttpResponse response;

    ResponseSpecImpl(RequestSpec<MockHttpRequest> requestSpec, MockHttpResponse response) {
        this.requestSpec = requestSpec;
        this.response = response;
    }

    public PromiseSpec<MockHttpResponse> then() {
        return this;
    }

    public ExtractableSpec<MockHttpResponse> extract() {
        return this;
    }

    public AssertableSpec<MockHttpResponse> body(Matcher<String> matcher) {
        Assert.assertThat(extract().contentAsString(), matcher);
        return this;
    }

    public <T> AssertableSpec<MockHttpResponse> body(String path, T exactValue) {
        return body(path, CoreMatchers.is(exactValue));
    }

    public <T> AssertableSpec<MockHttpResponse> body(String path, Matcher<T> matcher) {
        Assert.assertThat(extract().<T>path(path), matcher);
        return this;
    }

    public AssertableSpec<MockHttpResponse> isOk() {
        return statusIs(200);
    }

    public AssertableSpec<MockHttpResponse> statusIs(int status) {
        return statusIs(CoreMatchers.is(status));
    }

    public AssertableSpec<MockHttpResponse> statusIs(Matcher<Integer> matcher) {
        Assert.assertThat(extract().status(), matcher);
        return this;
    }

    public <T> AssertableSpec<MockHttpResponse> matchesHeaders(String header, T... values) {
        return matchesHeaders(header, CoreMatchers.hasItems(values));
    }

    public <T> AssertableSpec<MockHttpResponse> matchesHeaders(String header, Matcher<Iterable<T>> matcher) {
        Assert.assertThat(extract().<T>headers(header), matcher);
        return this;
    }

    public <T> AssertableSpec<MockHttpResponse> header(String header, T value) {
        return header(header, CoreMatchers.is(value));
    }

    public <T> AssertableSpec<MockHttpResponse> header(String header, Matcher<T> matcher) {
        Assert.assertThat(extract().<T>header(header), matcher);
        return this;
    }

    public AssertableSpec<MockHttpResponse> expect(String mediaType) {
        return expect(MediaType.valueOf(mediaType));
    }

    public AssertableSpec<MockHttpResponse> expect(MediaType mediaType) {
        return expect(CoreMatchers.is(mediaType));
    }

    public AssertableSpec<MockHttpResponse> expect(Matcher<MediaType> matcher) {
        Assert.assertThat(extract().contentType(), matcher);
        return this;
    }

    public AssertableSpec<MockHttpResponse> and() {
        return this;
    }

    public ExtractableSpec<MockHttpResponse> thenExtract() {
        return this;
    }

    public String contentAsString() {
        return response.getContentAsString();
    }

    public <T> T path(String path) {
        return JsonPath.from(contentAsString()).get(path);
    }

    public String pathAsString(String path) {
        return String.valueOf(path(path));
    }

    @SuppressWarnings("unchecked")
    public <T> T header(String header) {
        return (T) response.getOutputHeaders().getFirst(header);
    }

    @SuppressWarnings("unchecked")
    public <T> Collection<T> headers(String header) {
        return (Collection<T>) new ArrayList<Object>(response.getOutputHeaders().get(header));
    }

    public MediaType contentType() {
        return header(HEADER_CONTENT_TYPE);
    }

    public int status() {
        return response.getStatus();
    }

    public AssertableSpec<MockHttpResponse> assertThat() {
        return this;
    }
}
