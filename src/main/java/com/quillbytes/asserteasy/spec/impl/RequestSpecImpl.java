package com.quillbytes.asserteasy.spec.impl;

import com.quillbytes.asserteasy.spec.ResponseSpec;
import com.quillbytes.asserteasy.spec.SetupSpec;
import com.quillbytes.asserteasy.HttpMethod;
import com.quillbytes.asserteasy.spec.RequestSpec;
import org.jboss.resteasy.core.Dispatcher;
import org.jboss.resteasy.mock.MockDispatcherFactory;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.mock.MockHttpResponse;
import org.jboss.resteasy.plugins.server.resourcefactory.POJOResourceFactory;
import org.jboss.resteasy.spi.Registry;

import javax.ws.rs.core.MediaType;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class RequestSpecImpl implements RequestSpec<MockHttpRequest> {
    SetupSpec setupSpec;
    private MockHttpRequest request;
    private URI uri;

    RequestSpecImpl(SetupSpec setupSpec) {
        this.setupSpec = setupSpec;
    }

    private boolean hasAuthentication() {
        return setupSpec.hasAuthentication();
    }

    private Dispatcher createDispatcher() {
        Dispatcher dispatcher = MockDispatcherFactory.createDispatcher();
        Registry registry = dispatcher.getRegistry();
        Collection<Class<?>> serviceClasses = setupSpec.getServiceClasses();
        for (Class<?> serviceClass : serviceClasses) {
            POJOResourceFactory resourceFactory = new POJOResourceFactory(serviceClass);
            registry.addResourceFactory(resourceFactory);
        }
        return dispatcher;
    }

    private MockHttpRequest createRequest(HttpMethod method, URI uri) {
        MockHttpRequest request = MockHttpRequest.create(method.toString(), uri, null);
        request.accept(new ArrayList<MediaType>(setupSpec.getAcceptedTypes()));
        Map<String,String> parameters = setupSpec.getParameters();
        for (String key : parameters.keySet()) {
            request.addFormHeader(key, parameters.get(key));
        }
        this.request = request;
        this.uri = uri;
        return request;
    }

    private MockHttpResponse dispatchRequest(Dispatcher dispatcher, MockHttpRequest request) {
        MockHttpResponse response = new MockHttpResponse();
        dispatcher.invoke(request, response);
        return response;
    }

    public ResponseSpec<MockHttpResponse> get(String uri) {
        return get(URI.create(uri));
    }

    public ResponseSpec<MockHttpResponse> get(URI uri) {
        Dispatcher dispatcher = createDispatcher();
        MockHttpRequest request = createRequest(HttpMethod.GET, uri);
        MockHttpResponse response = dispatchRequest(dispatcher, request);
        return new ResponseSpecImpl(this, response);
    }

    public ResponseSpec<MockHttpResponse> post(String uri) {
        throw new UnsupportedOperationException();
    }

    public ResponseSpec<MockHttpResponse> post(URI uri) {
        throw new UnsupportedOperationException();
    }

    public ResponseSpec<MockHttpResponse> put(String uri) {
        throw new UnsupportedOperationException();
    }

    public ResponseSpec<MockHttpResponse> put(URI uri) {
        throw new UnsupportedOperationException();
    }

    public ResponseSpec<MockHttpResponse> patch(String uri) {
        throw new UnsupportedOperationException();
    }

    public ResponseSpec<MockHttpResponse> patch(URI uri) {
        throw new UnsupportedOperationException();
    }

    public ResponseSpec<MockHttpResponse> delete(String uri) {
        throw new UnsupportedOperationException();
    }

    public ResponseSpec<MockHttpResponse> delete(URI uri) {
        throw new UnsupportedOperationException();
    }

    public ResponseSpec<MockHttpResponse> head(String uri) {
        throw new UnsupportedOperationException();
    }

    public ResponseSpec<MockHttpResponse> head(URI uri) {
        throw new UnsupportedOperationException();
    }

    public ResponseSpec<MockHttpResponse> options(String uri) {
        throw new UnsupportedOperationException();
    }

    public ResponseSpec<MockHttpResponse> options(URI uri) {
        throw new UnsupportedOperationException();
    }

    public ResponseSpec<MockHttpResponse> uploadFile(String uri, File file) {
        throw new UnsupportedOperationException();
    }

    public ResponseSpec<MockHttpResponse> uploadFile(URI uri, File file) {
        throw new UnsupportedOperationException();
    }

    public MockHttpRequest getRequest() {
        return request;
    }

    public URI getURI() {
        return uri;
    }
}
