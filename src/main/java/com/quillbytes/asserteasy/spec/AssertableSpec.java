package com.quillbytes.asserteasy.spec;

import org.hamcrest.Matcher;

import javax.ws.rs.core.MediaType;

public interface AssertableSpec<AssertableType> {
    AssertableSpec<AssertableType> body(Matcher<String> matcher);
    <T> AssertableSpec<AssertableType> body(String path, T exactValue);
    <T> AssertableSpec<AssertableType> body(String path, Matcher<T> matcher);
    AssertableSpec<AssertableType> isOk();
    AssertableSpec<AssertableType> statusIs(int status);
    AssertableSpec<AssertableType> statusIs(Matcher<Integer> matcher);
    <T> AssertableSpec<AssertableType> matchesHeaders(String header, T... value);
    <T> AssertableSpec<AssertableType> matchesHeaders(String header, Matcher<Iterable<T>> matcher);
    <T> AssertableSpec<AssertableType> header(String header, T value);
    <T> AssertableSpec<AssertableType> header(String header, Matcher<T> matcher);
    AssertableSpec<AssertableType> expect(String mediaType);
    AssertableSpec<AssertableType> expect(MediaType mediaType);
    AssertableSpec<AssertableType> expect(Matcher<MediaType> matcher);
    AssertableSpec<AssertableType> and();
    ExtractableSpec<AssertableType> thenExtract();
}
