package com.quillbytes.asserteasy.spec;

public interface PromiseSpec<PromiseType> {
    AssertableSpec<PromiseType> assertThat();
    ExtractableSpec<PromiseType> extract();
}
