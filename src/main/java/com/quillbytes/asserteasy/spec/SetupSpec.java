package com.quillbytes.asserteasy.spec;

import javax.ws.rs.core.MediaType;
import java.util.Collection;
import java.util.Map;

public interface SetupSpec {
    SetupSpec service(Class<?> serviceClass);
    SetupSpec services(Class<?>... serviceClasses);
    SetupSpec accepts(String... mediaTypes);
    SetupSpec accepts(MediaType... mediaTypes);
    SetupSpec accepts(Collection<MediaType> mediaTypes);
    SetupSpec parameters(String... keyValueSequence);
    SetupSpec parameters(Map<String,String> values);
    SetupSpec params(String... keyValueSequence);
    SetupSpec params(Map<String,String> values);
    SetupSpec parameter(String key, String value);
    SetupSpec param(String key, String value);
    AuthenticatedSetupSpec auth();
    RequestSpec when();

    Collection<Class<?>> getServiceClasses();
    Collection<MediaType> getAcceptedTypes();
    Map<String,String> getParameters();
    boolean hasAuthentication();
}
