package com.quillbytes.asserteasy.spec;

public interface AuthenticatedSetupSpec extends SetupSpec {
    AuthenticatedSetupSpec basic(String username, String password);
}
