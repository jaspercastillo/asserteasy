package com.quillbytes.asserteasy.spec;

public interface ResponseSpec<ResponseType> {
    PromiseSpec<ResponseType> then();
}
