package com.quillbytes.asserteasy;

import com.quillbytes.asserteasy.spec.SetupSpec;
import com.quillbytes.asserteasy.spec.impl.SetupSpecImpl;

public class AssertEasy {
    public static SetupSpec given() {
        return new SetupSpecImpl();
    }
}
